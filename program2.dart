void main() {
  //a
  var win2012 = [
    "FNB Banking",
    "Health ID",
    "TransUnion Dealer Guide",
    "Rapidtargets",
    "Matchy",
    "Plascon Inspire Me",
    "PhraZApp",
  ];
  var win2013 = [
    "DStv",
    ".comm Telco Data Visualizer",
    "PriceCheck Mobile",
    "MarkitShare",
    "Nedbank App Suite",
    "SnapScan",
    "Kids Aid",
    "bookly",
    "Gautrain Buddy"
  ];
  var win2014 = [
    "SuperSport",
    "SyncMobile",
    "My Belongings",
    "LIVE Inspect",
    "Vigo",
    "Zapper",
    "Rea Vaya",
    "Wildlife tracker"
  ];
  var win2015 = [
    "VulaMobile",
    "DStv Now",
    "WumDrop",
    "CPUT Mobile",
    "EskomSePush",
    "M4JAM"
  ];
  var win2016 = [
    "Domestly",
    "iKhokha",
    "HearZA",
    "Tuta-me",
    "KaChing",
    "Friendly Math Monsters for Kindergarten"
  ];
  var win2017 = [
    "TransUnion 1Check",
    "OrderIN",
    "EcoSlips",
    "InterGreatMe",
    "Zulzi",
    "Hey Jude",
    "Oru Socia",
    "TouchSA",
    "Pick n Pay Super Animals 2",
    "The TreeApp South Africa",
    "WatIf Health Portal",
    "Awethu Project",
    "Shyft"
  ];
  var win2018 = [
    "Pineapple",
    "Cowa Bunga",
    "Digemy Knowledge Partner and Besmarter",
    "Bestee",
    "The African Cyber Gaming League App (ACGL)",
    "dbTrack",
    "Bestee",
    "Stokfella",
    "Difela Hymns",
    "Xander English 1-20",
    "Ctrl",
    "Khula",
    "ASI Snakes",
    "Khula"
  ];
  var win2019 = [
    "Digger",
    "SI Realities",
    "Vula Mobile",
    "Hydra Farm",
    "Matric Live",
    "Franc",
    "Over",
    "LocTransi",
    "Naked Insurance",
    "LocTransi",
    "Loot Defence",
    "MoWash"
  ];
  var win2020 = [
    "EasyEquities",
    "Checkers Sixty60",
    "Technishen",
    "BirdPro",
    "Lexie Hearing",
    "GreenFingers Mobile",
    "Xitsonga Dictionary",
    "StokFella",
    "Bottles",
    "Matric Live",
    "Guardian Health",
    "Phanda Ispani",
    "Scan & Go",
    "My Pregnancy Journey"
  ];
  var win2021 = [
    "iiDENTIFii",
    "Hellopay SoftPOS",
    "Guardian Health Platform",
    "Ambani Africa",
    "Murimi",
    "Shyft",
    "Sisa",
    "UniWise",
    "Kazi",
    "Takealot",
    "Rekindle Learning",
    "Roadsave",
    "Afrihost"
  ];

  win2012.sort();
  win2013.sort();
  win2014.sort();
  win2015.sort();
  win2016.sort();
  win2017.sort();
  win2018.sort();
  win2019.sort();
  win2020.sort();
  win2021.sort();

  print("Winners of 2012 are: $win2012");
  print("Winners of 2013 are: $win2013");
  print("Winners of 2014 are: $win2014");
  print("Winners of 2015 are: $win2015");
  print("Winners of 2016 are: $win2016");
  print("Winners of 2017 are: $win2017");
  print("Winners of 2018 are: $win2018");
  print("Winners of 2019 are: $win2019");
  print("Winners of 2020 are: $win2020");
  print("Winners of 2021 are: $win2021");

  //b
  print("Winning app for 2017 is: Shyft");
  print("Winning app for 2018 is: Khula ecosystem");

  //c
  var len = win2012.length +
      win2013.length +
      win2014.length +
      win2015.length +
      win2016.length +
      win2017.length +
      win2018.length +
      win2019.length +
      win2020.length +
      win2021.length;
  print("The number of winnings apps from 2012-2021 is $len");
}
