void main() {
  var app = new App();
  app.name = "Uber";
  app.sector = "Transportation";
  app.developer = "Garrett Camp";
  app.winYear = 2017;

  app.printAppInfo();
  app.printAppNameCaps();
}

class App {
  String? name;
  String? sector;
  String? developer;
  int? winYear;

  void printAppInfo() {
    print(
        "The name of the App is $name. The sector the App is in is $sector. The developer is $developer. The year it won $winYear");
  }

  void printAppNameCaps() {
    print(name?.toUpperCase());
  }
}
